package org.nihilisto.eclipse.launcher.composite.ui;

import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.ICheckStateListener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConfigurationsCheckStateListener implements ICheckStateListener
{
	private final CheckboxTreeViewer viewer;
	private final ConfigurationsTreeContentProvider contentProvider;
	private final Map<Object, Integer> parentCheckedChildrenCount;

	public ConfigurationsCheckStateListener(CheckboxTreeViewer viewer, ConfigurationsTreeContentProvider contentProvider)
	{
		this.viewer = viewer;
		this.contentProvider = contentProvider;
		this.parentCheckedChildrenCount = new HashMap<>();
	}

	public void init(ConfigurationsExpert expert, List<String> checkedConfigurationNames)
	{
		for (String name : checkedConfigurationNames)
		{
			Object child = expert.getConfiguration(name);

			if (child == null)
			{
				continue;
			}

			Object parent = contentProvider.getParent(child);
			viewer.setChecked(child, true);
			checkChild(parent, child, true);
		}
	}

	@Override
	public void checkStateChanged(CheckStateChangedEvent event)
	{
		Object parent = contentProvider.getParent(event.getElement());

		if (parent == null)
		{
			checkParent(event.getElement(), event.getChecked());
		}
		else
		{
			checkChild(parent, event.getElement(), event.getChecked());
		}

		afterCheckStateChanged();
	}

	private void checkParent(Object parent, boolean checked)
	{
		if (checked)
		{
			viewer.expandToLevel(parent, AbstractTreeViewer.ALL_LEVELS);
		}

		viewer.setGrayed(parent, false);
		viewer.setSubtreeChecked(parent, checked);
		parentCheckedChildrenCount.put(parent, checked ? contentProvider.getChildren(parent).length : 0);
	}

	private void checkChild(Object parent, Object child, boolean checked)
	{
		Integer count = parentCheckedChildrenCount.getOrDefault(parent, 0) + (checked ? 1 : -1);
		parentCheckedChildrenCount.put(parent, count);

		if (count == 0)
		{
			viewer.setGrayed(parent, false);
			viewer.setChecked(parent, false);
		}
		else if (count == contentProvider.getChildren(parent).length)
		{
			viewer.setGrayed(parent, false);
			viewer.setChecked(parent, true);
		}
		else
		{
			viewer.setGrayed(parent, true);
			viewer.setChecked(parent, true);
		}
	}

	public void afterCheckStateChanged() {}
}
