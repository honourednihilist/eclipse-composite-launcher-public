package org.nihilisto.eclipse.launcher.composite.ui;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;

import java.util.*;

public class ConfigurationsExpert
{
	private final SortedMap<String, ILaunchConfiguration> configurations;
	private final SortedMap<ILaunchConfigurationType, SortedSet<ILaunchConfiguration>> configurationsByType;

	public ConfigurationsExpert(ILaunchConfiguration[] availableConfigurations, ILaunchConfiguration excludedConfiguration) throws CoreException
	{
		configurations = new TreeMap<>();
		configurationsByType = new TreeMap<>((t1, t2) -> t1.getName().compareTo(t2.getName()));

		for (ILaunchConfiguration conf : availableConfigurations)
		{
			if (conf.getName().equals(excludedConfiguration.getName()))
			{
				continue;
			}

			configurations.put(conf.getName(), conf);

			ILaunchConfigurationType type = conf.getType();
			SortedSet<ILaunchConfiguration> typeConfigurations = configurationsByType.get(type);

			if (typeConfigurations == null)
			{
				typeConfigurations = new TreeSet<>((c1, c2) -> c1.getName().compareTo(c2.getName()));
				configurationsByType.put(type, typeConfigurations);
			}

			typeConfigurations.add(conf);
		}
	}

	public Set<ILaunchConfigurationType> getTypes()
	{
		return configurationsByType.keySet();
	}

	public SortedMap<ILaunchConfigurationType, SortedSet<ILaunchConfiguration>> getConfigurationsByType()
	{
		return configurationsByType;
	}

	public Set<String> getConfigurationNames()
	{
		return configurations.keySet();
	}

	public ILaunchConfiguration getConfiguration(String name)
	{
		return configurations.get(name);
	}
}
