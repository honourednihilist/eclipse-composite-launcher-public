package org.nihilisto.eclipse.launcher.composite.ui;

import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.debug.ui.IDebugModelPresentation;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;

public class ConfigurationsLabelProvider extends LabelProvider
{
	private IDebugModelPresentation presentation;
	private Map<Object, Image> elementImageMap;
	private Map<Object, String> elementTextMap;

	public void init(ConfigurationsExpert expert)
	{
		presentation = DebugUITools.newDebugModelPresentation();
		elementImageMap = new HashMap<>();
		elementTextMap = new HashMap<>();

		for (Entry<ILaunchConfigurationType, SortedSet<ILaunchConfiguration>> e : expert.getConfigurationsByType().entrySet())
		{
			ILaunchConfigurationType type = e.getKey();
			Image image = presentation.getImage(type);
			elementImageMap.put(type, image);
			elementTextMap.put(type, type.getName());
			e.getValue().forEach(c ->
			{
				elementImageMap.put(c, image);
				elementTextMap.put(c, c.getName());
			});
		}
	}

	@Override
	public Image getImage(Object element)
	{
		return elementImageMap.get(element);
	}

	@Override
	public String getText(Object element)
	{
		return elementTextMap.get(element);
	}

	@Override
	public void dispose()
	{
		super.dispose();
		presentation.dispose();
	}
}
