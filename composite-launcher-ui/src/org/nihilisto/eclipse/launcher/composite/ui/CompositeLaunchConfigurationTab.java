package org.nihilisto.eclipse.launcher.composite.ui;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.LocalResourceManager;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.nihilisto.eclipse.launcher.composite.CompositeLaunchConfigurationDelegate.ATTR_SELECTED_CONFIGURATIONS;

public class CompositeLaunchConfigurationTab extends AbstractLaunchConfigurationTab
{
	private Image image;
	private CheckboxTreeViewer viewer;
	private ConfigurationsTreeContentProvider contentProvider;
	private ConfigurationsLabelProvider labelProvider;
	private ConfigurationsCheckStateListener checkStateListener;
	private ConfigurationsExpert expert;

	@Override
	public String getName()
	{
		return "Configurations";
	}

	@Override
	public Image getImage()
	{
		return image;
	}

	@Override
	public void createControl(Composite parent)
	{
		Bundle bundle = FrameworkUtil.getBundle(this.getClass());
		URL url = FileLocator.find(bundle, new Path("icons/composite-launcher-tab-icon.png"), null);
		ImageDescriptor imageDescriptor = ImageDescriptor.createFromURL(url);
		image = new LocalResourceManager(JFaceResources.getResources(), parent).createImage(imageDescriptor);

		Composite root = new Composite(parent, SWT.NONE);
		root.setLayout(new GridLayout());

		new Label(root, SWT.NONE).setText("Select configurations to launch:");

		viewer = new CheckboxTreeViewer(root, SWT.BORDER);
		viewer.getTree().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		viewer.setContentProvider(contentProvider = new ConfigurationsTreeContentProvider());
		viewer.setLabelProvider(labelProvider = new ConfigurationsLabelProvider());
		viewer.addCheckStateListener(checkStateListener = new ConfigurationsCheckStateListener(viewer, contentProvider)
		{
			@Override
			public void afterCheckStateChanged()
			{
				updateLaunchConfigurationDialog();
			}
		});

		setControl(root);
	}

	@Override
	public void setDefaults(ILaunchConfigurationWorkingCopy configuration)
	{
	}

	@Override
	public void initializeFrom(ILaunchConfiguration configuration)
	{
		List<String> selectedConfigurationNames;
		try
		{
			selectedConfigurationNames = configuration.getAttribute(ATTR_SELECTED_CONFIGURATIONS, Collections.emptyList());
			expert = new ConfigurationsExpert(getLaunchManager().getLaunchConfigurations(), configuration);
		}
		catch (CoreException e)
		{
			setErrorMessage(e.getMessage());
			return;
		}

		contentProvider.init(expert);
		labelProvider.init(expert);
		viewer.setInput(expert);
		checkStateListener.init(expert, selectedConfigurationNames);
		viewer.expandAll();
	}

	@Override
	public void performApply(ILaunchConfigurationWorkingCopy configuration)
	{
		configuration.setAttribute(ATTR_SELECTED_CONFIGURATIONS, getCheckedConfigurationNames());
	}

	@Override
	public boolean canSave()
	{
		validate(getCheckedConfigurationNames());
		return getErrorMessage() == null;
	}

	private List<String> getCheckedConfigurationNames()
	{
		return Arrays.stream(viewer.getCheckedElements())
				.filter(e -> e instanceof ILaunchConfiguration)
				.map(e -> ((ILaunchConfiguration) e).getName())
				.sorted()
				.collect(Collectors.toList());
	}

	@Override
	public boolean isValid(ILaunchConfiguration launchConfig)
	{
		try
		{
			validate(launchConfig.getAttribute(ATTR_SELECTED_CONFIGURATIONS, Collections.emptyList()));
		}
		catch (CoreException e)
		{
			setErrorMessage(e.getMessage());
		}

		return getErrorMessage() == null;
	}

	private void validate(Collection<String> selectedConfigs)
	{
		setErrorMessage(null);

		if (selectedConfigs == null || selectedConfigs.isEmpty())
		{
			return;
		}

		for (String name : selectedConfigs)
		{
			if (!expert.getConfigurationNames().contains(name))
			{
				setErrorMessage("Configuration '" + name + "' does not exist");
				return;
			}
		}
	}
}
