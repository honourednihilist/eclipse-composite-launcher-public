package org.nihilisto.eclipse.launcher.composite.ui;

import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

import java.util.*;
import java.util.Map.Entry;

public class ConfigurationsTreeContentProvider implements ITreeContentProvider
{
	private Object[] rootElements;
	private Map<Object, Object[]> parentChildrenMap;
	private Map<Object, Object> childParentMap;

	public void init(ConfigurationsExpert expert)
	{
		rootElements = expert.getTypes().toArray();

		SortedMap<ILaunchConfigurationType, SortedSet<ILaunchConfiguration>> configurationsByType = expert.getConfigurationsByType();
		parentChildrenMap = new HashMap<>(configurationsByType.size());
		childParentMap = new HashMap<>(configurationsByType.size());

		for (Entry<ILaunchConfigurationType, SortedSet<ILaunchConfiguration>> e : configurationsByType.entrySet())
		{
			Object parent = e.getKey();
			Object[] children = e.getValue().toArray();
			parentChildrenMap.put(parent, children);
			Arrays.stream(children).forEach(c -> childParentMap.put(c, parent));
		}
	}

	@Override
	public Object[] getElements(Object inputElement)
	{
		return rootElements;
	}

	@Override
	public boolean hasChildren(Object element)
	{
		return parentChildrenMap.containsKey(element);
	}

	@Override
	public Object[] getChildren(Object parentElement)
	{
		return parentChildrenMap.get(parentElement);
	}

	@Override
	public Object getParent(Object element)
	{
		return childParentMap.get(element);
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {}

	@Override
	public void dispose() {}
}
