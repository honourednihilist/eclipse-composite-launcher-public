package org.nihilisto.eclipse.launcher.composite;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.core.model.ILaunchConfigurationDelegate;

public class CompositeLaunchConfigurationDelegate implements ILaunchConfigurationDelegate 
{
	public static String LAUNCH_CONFIGURATION_TYPE = CompositeLaunchConfigurationDelegate.class.getPackage().getName() + ".CompositeLaunchConfigurationType";
	public static String ATTR_SELECTED_CONFIGURATIONS = "selected.configurations";
	
	@Override
	public void launch(ILaunchConfiguration configuration, String mode, ILaunch launch, IProgressMonitor monitor) throws CoreException
	{
		if (!configuration.getType().getIdentifier().equals(LAUNCH_CONFIGURATION_TYPE))
		{
			throw new CoreException(new Status(IStatus.ERROR, LAUNCH_CONFIGURATION_TYPE, "Unsupported launch configuration type"));
		}
		
		if (!ILaunchManager.RUN_MODE.equals(mode) && !ILaunchManager.DEBUG_MODE.equals(mode))
		{
			throw new CoreException(new Status(IStatus.ERROR, LAUNCH_CONFIGURATION_TYPE, "Unsupported launch mode"));
		}
		
		ILaunchConfiguration[] configurations = DebugPlugin.getDefault().getLaunchManager().getLaunchConfigurations();
		Map<String, ILaunchConfiguration> configurationsMap = Arrays.stream(configurations)
				.collect(Collectors.toMap(ILaunchConfiguration::getName, Function.<ILaunchConfiguration>identity()));
		
		List<ILaunchConfiguration> configurationsToLaunch = new ArrayList<>();
		
		for (String name : configuration.getAttribute(ATTR_SELECTED_CONFIGURATIONS, Collections.emptyList()))
		{
			ILaunchConfiguration cfg = configurationsMap.get(name);
			
			if (cfg == null)
			{
				throw new CoreException(new Status(IStatus.ERROR, LAUNCH_CONFIGURATION_TYPE, "Configuration '" + name + "' does not exist"));
			}
			
			configurationsToLaunch.add(cfg);
		}
		
		launchInternal(configuration.getName(), configurationsToLaunch, mode, monitor != null ? monitor : new NullProgressMonitor());
	}
	
	private void launchInternal(String task, List<ILaunchConfiguration> configs, String mode, IProgressMonitor monitor) throws CoreException
	{
		if (configs.isEmpty())
		{
			return;
		}

		monitor.beginTask(task, configs.size());
		try
		{
			for (ILaunchConfiguration cfg : configs)
			{
				if (monitor.isCanceled())
				{
					return;
				}
				
				monitor.subTask(cfg.getName());
				cfg.launch(mode, null);
				monitor.worked(1);
			}
		}
		finally
		{
			monitor.done();
		}
	}
}
